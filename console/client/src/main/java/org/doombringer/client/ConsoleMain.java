package org.doombringer.client;

import java.io.File;
import java.io.IOException;
import jline.TerminalSupport;
import jline.console.ConsoleReader;
import jline.console.history.FileHistory;
import jline.internal.Configuration;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.doombringer.api.Console;
import org.jboss.resteasy.client.ProxyFactory;

public class ConsoleMain {
    public static final String QUIT_CMD = "quit";
    public static final String NEXT_LINE = "\\";

    public static void main(String[] args) {
        ConnectionInfo info = getConnectionInfo(args);
        Console console = ProxyFactory.create(Console.class, info.getUrl());

        System.out.println(String.format("Connected to '%s'", info.getUrl()));
        System.out.println(String.format("Type '%s' to exit", QUIT_CMD));

        try {
            String prompt = String.format("[%s]", console.getLang() + ":");

            ConsoleReader consoleReader = new ConsoleReader(System.in, System.out, new TerminalSupport(false) {
            });

            consoleReader.setHistory(new FileHistory(new File(Configuration.getUserHome(),
                    String.format(".jline-%s.history", ConsoleMain.class))));

            consoleReader.setHistoryEnabled(true);
            StringBuilder command = new StringBuilder();
            while (true) {
                String str = consoleReader.readLine(prompt);
                str = str.trim();

                if (QUIT_CMD.equalsIgnoreCase(str)) {
                    System.out.println("Bye!");
                    System.exit(0);
                }

                try {
                    command.append(str);
                    if (str.endsWith(NEXT_LINE)) {
                        command.setLength(command.length() - 1);
                    } else {
                        String response = console.executeCommand(command.toString());
                        command.setLength(0);
                        System.out.println(response);
                    }
                } catch (Exception e) {
                    System.out.println("Failed to execute command: " + e.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ConnectionInfo getConnectionInfo(String[] args) {
        Options options = createOptions();
        CommandLineParser parser = new BasicParser();
        ConnectionInfo info = new ConnectionInfo();

        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("p")) {
                info.port = Integer.parseInt(cmd.getOptionValue("p"));
            }

            if (cmd.hasOption("h")) {
                info.host = cmd.getOptionValue("h");
            }

            info.useSSl = cmd.hasOption("ssl");
        } catch (ParseException e) {
            System.out.println("Failed to parse command line");
            System.exit(-1);
        }

        return info;
    }

    private static Options createOptions() {
        Options o = new Options();
        o.addOption("p", true, "Port to connect process console");
        o.addOption("h", true, "Hostname of process console");
        o.addOption("ssl", false, "Use secured channel (ssl)");

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -cp client.jar " + ConsoleMain.class.getName(), o);
        System.out.println("\n");

        return o;
    }

    private static class ConnectionInfo {
        int port = 8081;
        String host = "localhost";
        boolean useSSl = false;

        public String getUrl() {
            return String.format("http%s://%s:%s/", (useSSl ? "s" : ""), host, port);
        }
    }
}
