package org.doombringer.sample;

import org.doombringer.api.Server;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created: 6/26/13
 */
public class SampleMain {
    public static void main(String[] args) throws InterruptedException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("sample-context.xml");
        Server server = ctx.getBean(Server.class);
        server.await();
    }
}
