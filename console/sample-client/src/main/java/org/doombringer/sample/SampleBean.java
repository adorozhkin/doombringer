package org.doombringer.sample;

/**
 * Created: 6/26/13
 */
public class SampleBean {
    private String name;

    public SampleBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String hello(String prefix) {
        return prefix + " " + name;
    }
}
