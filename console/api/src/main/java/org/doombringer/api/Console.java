package org.doombringer.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/console")
@Produces({MediaType.TEXT_PLAIN})
@Consumes({MediaType.TEXT_PLAIN})
public interface Console {
    @POST
    @Path("/command")
    String executeCommand(String command);

    @GET
    @Path("/lang")
    String getLang();
}
