package org.doombringer.api;

/**
 * Created: 6/26/13
 */
public interface Server {
    void start();

    void await() throws InterruptedException;

    void shutdown();
}
