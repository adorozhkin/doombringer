package org.doombringer.api;

import java.util.Map;

public interface Executor {
    void setEntryPoints(Map<String, Object> entryPoints);

    String execute(String cmd);

    String getLang();
}
