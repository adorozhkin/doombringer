(ns doombringer.console
  (:import (clojure.lang)))

(println "Loading console bindings")
(def ctx)

(defn anton [] (println "Hello master!"))

(defmacro bean [name] `(. ctx getBean (str (quote ~name))))

(defn beans [] (. ctx getBeanDefinitionNames))

(defn print-beans []
  (loop [c (beans)]
    (if (empty? c)
      nil
      (do (println (str (first c)))
        (recur (rest c))))))