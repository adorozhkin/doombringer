package org.doombringer.clojure.impl;

import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created: 6/26/13
 */
public class StringQueueWriter extends Writer {
    private LinkedBlockingQueue<String> output = new LinkedBlockingQueue<>();

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        output.add(new String(cbuf, off, len));
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void close() throws IOException {
    }

    public int size() {
        return output.size();
    }

    public String poll() {
        return output.poll();
    }
}
