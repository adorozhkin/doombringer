package org.doombringer.clojure.impl;

import clojure.lang.Namespace;
import clojure.lang.RT;
import clojure.lang.Symbol;
import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import org.doombringer.api.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class ClojureExecutor implements Executor, InitializingBean, ApplicationContextAware {
    public static final String DOOMBRINGER_NS = "doombringer.console";
    private Map<String, Object> entryPoints;
    private ApplicationContext ctx;
    private Logger log = LoggerFactory.getLogger(ClojureExecutor.class);

    private final StringQueueWriter clojureOutput = new StringQueueWriter();

    @Override
    public void afterPropertiesSet() throws Exception {
        RT.CURRENT_NS.doReset(Namespace.findOrCreate(Symbol.intern(DOOMBRINGER_NS)));
        RT.load("default");
        RT.var("clojure.core", "*out*", clojureOutput);
        RT.var(DOOMBRINGER_NS, "ctx", ctx);

        if (entryPoints != null) {
            for (Map.Entry<String, Object> entry : entryPoints.entrySet()) {
                RT.var(DOOMBRINGER_NS, entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public void setEntryPoints(Map<String, Object> entryPoints) {
        this.entryPoints = entryPoints;
    }

    @Override
    public String execute(String cmd) {
        StringReader reader = new StringReader(cmd);
        try {
            Object result = clojure.lang.Compiler.load(reader);
            StringBuilder sb = readClojureResponse();
            sb.append(String.valueOf(result));

            return sb.toString();
        } catch (Exception e) {
            log.error("Failed to execute command {} : {}", new Object[]{cmd, e.getMessage()}, e);
            try {
                StringBuilder sb = readClojureResponse();
                sb.append(e.getMessage());
                return sb.toString();
            } catch (IOException e1) {
                log.error("Failed to read response", e1);
                return e.getMessage();
            }
        }
    }

    @Override
    public String getLang() {
        return "clojure";
    }

    private StringBuilder readClojureResponse() throws IOException {
        StringBuilder sb = new StringBuilder();

        while (clojureOutput.size() > 0) {
            sb.append(clojureOutput.poll());
        }

        return sb;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}