package org.doombringer.server;

import java.util.Set;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import org.reflections.Reflections;

public class ResteasyApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Reflections reflections = new Reflections("org.doombringer.server");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Path.class);
        return annotated;
    }
}
