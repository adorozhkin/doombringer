package org.doombringer.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.doombringer.api.Console;
import org.doombringer.api.Executor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Path("/console")
@Service
@Produces({MediaType.TEXT_PLAIN})
@Consumes({MediaType.TEXT_PLAIN})
public class ConsoleResource implements Console {
    @Autowired
    private Executor executor;

    @Override
    @POST
    @Path("/command")
    public String executeCommand(String command) {
        return executor.execute(command);
    }

    @Override
    public String getLang() {
        return executor.getLang();
    }

    public void setExecutor(Executor executor) {
        this.executor = executor;
    }
}
