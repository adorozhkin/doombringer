package org.doombringer.server.impl;

import java.net.URL;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.doombringer.api.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created: 6/26/13
 */
public class JettyServer implements Server {
    private Logger log = LoggerFactory.getLogger(JettyServer.class);

    private int port;
    private org.eclipse.jetty.server.Server server;

    @Override
    @PostConstruct
    public void start() {
        try {
            this.server = new org.eclipse.jetty.server.Server(port);

            WebAppContext context = new WebAppContext();
            URL webXml = this.getClass().getClassLoader().getResource("WEB-INF/web.xml");
            context.setDescriptor(webXml.toExternalForm());
            context.setResourceBase(".");
            context.setContextPath("/");
            context.setParentLoaderPriority(true);

            server.setHandler(context);
            server.start();
        } catch (Exception e) {
            log.error("Fail to initialize Jetty server", e);
        }
    }

    @Override
    public void await() throws InterruptedException {
        server.join();
    }

    @Override
    @PreDestroy
    public void shutdown() {
        try {
            server.stop();
        } catch (Exception e) {
            log.error("Failed to stop Jetty server", e);
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
